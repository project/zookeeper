<?php

namespace Drupal\Zookeeper\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_zookeeper",
 *   title = @Translation("Zookeeper Filter"),
 *   description = @Translation("Text filter that change all animals words into emoji."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterZookeeper extends FilterBase {
  public function process($text, $langcode) {
    $animals = [
      'Dog' => '🐕',
      'Cat' => '🐈',
      'Monkey' => '🐒',
      'Bear' => '🐻',
      'Gorilla' => '🦍',
      'Orangutan' => '🦧',
      'Wolf' => '🐺',
      'Fox' => '🦊',
      'Raccoon' => '🦝',
      'Lion' => '🦁',
      'Tiger' => '🐅',
      'Leopard' => '🐆',
      'Horse' => '🐎',
      'Unicorn' => '🦄',
      'Zebra' => '🦓',
      'Deer' => '🦌',
      'Bison' => '🦌',
      'Ox' => '🐂',
      'Cow' => '🐄',
      'Pig' => '🐄',
      'Boar' => '🐗',
      'Ram' => '🐏',
      'Ewe' => '🐑',
      'Goat' => '🐐',
      'Camel' => '🐪',
      'Llama' => '🦙',
      'Giraffe' => '🦒',
      'Elephant' => '🐘',
      'Mammoth' => '🦣',
      'Rhinoceros' => '🦏',
      'Hippopotamus' => '🦛',
      'Mouse' => '🐁',
      'Chipmunk' => '🐿️',
      'Rabbit' => '🐇',
      'Beaver' => '🦫',
      'Hedgehog' => '🦔',
      'Bat' => '🦇',
      'Koala' => '🐨',
      'Panda' => '🐼',
      'Sloth' => '🦥',
      'Otter' => '🦦',
      'Skunk' => '🦨',
      'Kangaroo' => '🦘',
      'Badger' => '🦡',
      'Turkey' => '🦃',
      'Chicken' => '🐔',
      'Rooster' => '🐓',
      'Bird' => '🐦',
      'Penguin' => '🐧',
      'Dove' => '🕊️',
      'Eagle' => '🦅',
      'Duck' => '🦆',
      'Swan' => '🦢',
      'Owl' => '🦉',
      'Dodo' => '🦤',
      'Flamingo' => '🦩',
      'Peacock' => '🦚',
      'Parrot' => '🦜',
      'Frog' => '🐸',
      'Crocodile' => '🐊',
      'Turtle' => '🐢',
      'Lizard' => '🦎',
      'Snake' => '🐍',
      'T-Rex' => '🦖',
      'Whale' => '🐋',
      'Dolphin' => '🐬',
      'Seal' => '🦭',
      'Fish' => '🐟',
      'Blowfish' => '🐡',
      'Shark' => '🦈',
      'Octopus' => '🐙',
      'Snail' => '🐌',
      'Butterfly' => '🦋',
      'Bug' => '🐛',
      'Ant' => '🐜'
    ];
    return new FilterProcessResult(str_ireplace(array_keys($animals), array_values($animals), $text));
  }
}
